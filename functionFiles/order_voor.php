<?php
include('../db.php');
if(isset($_POST['voor'])) {
    $voor = $_POST['voor'];
    $sql = "SELECT hoeveel, fk_voor FROM bestellingen WHERE fk_voor = $voor AND bevestigd = 0";
    $result = $con->prepare($sql);
    $result->execute();
    $rows_bestelling = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $fk_voor = $row['fk_voor'];
        $hoeveel = $row['hoeveel'];
    }
    if($fk_voor != NULL) {
        $hoeveel = $hoeveel + 1;
        $sql = "UPDATE bestellingen SET hoeveel = :hoeveel WHERE fk_voor = $voor";
        $result = $con->prepare($sql);
        $result->bindParam(':hoeveel', $hoeveel);
        $result->execute();
    }
    else
    {
        $sql = "INSERT INTO bestellingen(fk_tafel_id, fk_voor, hoeveel) VALUES(:tafel, :voor, 1)";
        $result = $con->prepare($sql);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->bindParam(':voor', $voor);
        $result->execute();
    }
}
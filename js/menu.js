function tafel()
{
    var tafel=$("#tafel_nummer").val();
    if($.trim(tafel)!="")
    {
        $.ajax
        ({
            type:'post',
            url:'functionFiles/set_table.php',
            data: {
                    tafel:tafel
                },
            success:function(response) {
                if(response=="nummer")
                {
                    document.getElementById("alert_tafel").innerHTML = "Dat is geen getal.";
                }
                if(response=="dubbel")
                {
                    document.getElementById("alert_tafel").innerHTML = "Deze tafel is al in gebruik.";
                }
                if(response=="success")
                {
                    loadHeader();
                }
            }
        });
    }

    else
    {
        document.getElementById("alert_tafel").innerHTML = "Voer een tafelnummer in.";
    }

    return false;
}

function betaal() {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/unset_table.php',
        success:function(response) {
            if(response=="success")
            {
                div_hide2();
            }
        }
    });
}

function betaal_anders(betaal) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/betaal.php',
        data: {
                betaal:betaal
            },
        success:function(response) {
            if(response=="success")
            {
                location.reload();
            }
        }
    });
}

function loadHeader() {
    $('#header').load(document.URL + ' #header');
}

function bestel_drank(drank) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/order_drank.php',
        data: {
                drank:drank
            },
    });
}

function bestel_voor(voor) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/order_voor.php',
        data: {
                voor:voor
            },
    });
}

function bestel_hoofd(hoofd) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/order_hoofd.php',
        data: {
                hoofd:hoofd
            },
    });
}

function bestel_na(na) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/order_na.php',
        data: {
                na:na
            },
    });
}

function bestellen() {
    $('#lijst').load(document.URL + ' #lijst');
    document.getElementById('bestelling').style.display = "block";
    document.getElementById('menu').style.display = "none";
}

function betalen() {
    $('#lijst2').load(document.URL + ' #lijst2');
    document.getElementById('betalen').style.display = "block";
    document.getElementById('menu').style.display = "none";
}

function bevestigen() {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/order.php',
        success:function(response) {
            if(response=="success")
            {
                $('#lijst').load(document.URL + ' #lijst');
            }
        }
    });
}

function div_hide(){
document.getElementById('bestelling').style.display = "none";
document.getElementById('menu').style.display = "block";
}

function div_hide2(){
loadHeader();
document.getElementById('betalen').style.display = "none";
document.getElementById('menu').style.display = "block";
}

function drank(){
    document.getElementById('div_hoofd').style.display = "none";
    document.getElementById('div_voor').style.display = "none";
    document.getElementById('div_na').style.display = "none";
    document.getElementById('div_drank').style.display = "block";
}

function voor(){
    document.getElementById('div_hoofd').style.display = "none";
    document.getElementById('div_drank').style.display = "none";
    document.getElementById('div_na').style.display = "none";
    document.getElementById('div_voor').style.display = "block";
}

function hoofd(){
    document.getElementById('div_drank').style.display = "none";
    document.getElementById('div_voor').style.display = "none";
    document.getElementById('div_na').style.display = "none";
    document.getElementById('div_hoofd').style.display = "block";
}

function na(){
    document.getElementById('div_hoofd').style.display = "none";
    document.getElementById('div_voor').style.display = "none";
    document.getElementById('div_drank').style.display = "none";
    document.getElementById('div_na').style.display = "block";
}

function menu() {
    document.getElementById('div_hoofd').style.display = "block";
    document.getElementById('div_voor').style.display = "block";
    document.getElementById('div_drank').style.display = "block";
    document.getElementById('div_na').style.display = "block";
}

function change(id, getal, huidig) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/change.php',
        data: {
                id:id,
                getal:getal,
                huidig:huidig
            },
        success:function(response) {
            if(response=="success")
            {
                $('#lijst').load(document.URL + ' #lijst');
            }
        }
    });
}
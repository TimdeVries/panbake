<?php
session_start();
$host="127.0.0.1";
$socket="";
$user="root";
$password="";
$dbname="panbake";
$charset = 'utf8mb4';

if(isset($_SESSION['tafel_nr'])) {
    $_SESSION['tafel_nr'] = $_SESSION['tafel_nr'];
}

$dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
$opt = [
PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
PDO::ATTR_EMULATE_PREPARES   => false
];
try {
    global $con;
    $con = new PDO($dsn, $user, $password, $opt);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    exit;
}




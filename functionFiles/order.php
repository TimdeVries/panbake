<?php
include('../db.php');
$date = date('Y-m-d H:i:s');
$sql = "UPDATE bestellingen SET bevestigd = 1, tijd = :tijd WHERE fk_tafel_id = :tafel AND bevestigd = 0";
$result = $con->prepare($sql);
$result->bindParam(':tafel', $_SESSION['tafel_nr']);
$result->bindParam(':tijd', $date);
$result->execute();
echo "success";
<?php
include('../db.php');
if(isset($_POST['hoofd'])) {
    $hoofd = $_POST['hoofd'];
    $sql = "SELECT hoeveel, fk_hoofd FROM bestellingen WHERE fk_hoofd = $hoofd AND bevestigd = 0";
    $result = $con->prepare($sql);
    $result->execute();
    $rows_bestelling = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $fk_hoofd = $row['fk_hoofd'];
        $hoeveel = $row['hoeveel'];
    }
    if($fk_hoofd != NULL) {
        $hoeveel = $hoeveel + 1;
        $sql = "UPDATE bestellingen SET hoeveel = :hoeveel WHERE fk_hoofd = $hoofd";
        $result = $con->prepare($sql);
        $result->bindParam(':hoeveel', $hoeveel);
        $result->execute();
    }
    else
    {
        $sql = "INSERT INTO bestellingen(fk_tafel_id, fk_hoofd, hoeveel) VALUES(:tafel, :hoofd, 1)";
        $result = $con->prepare($sql);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->bindParam(':hoofd', $hoofd);
        $result->execute();
    }
}
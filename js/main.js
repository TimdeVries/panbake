function logout() {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/logout.php',
        success:function(response) {
            if(response=="success")
            {
                refresh();
            }
        }
    });
}

function bestel_keuken(keuken) {
    $.ajax
    ({
        type:'post',
        url:'functionFiles/order_keuken.php',
        data: {
                keuken:keuken
            },
            success:function(response) {
            if(response=="success")
            {
                refresh();
            }
        }
    });
}
function refresh() {
    $('#body').load(document.URL + ' #body');
}

function do_login() {
    var user=$("#user").val();
    var pass=$("#pass").val();
    if($.trim(user)!="" && $.trim(pass)!="")
    {
        $.ajax
        ({
            type:'post',
            url:'functionFiles/login.php',
            data: {
                    user:user,
                    pass:pass
                },
            success:function(response) {
                if(response=="success")
                {
                    location.reload();
                }
                else if (response == "fail") {
                    document.getElementById("alert3").innerHTML = "Ongeldige gebruikersnaam en/of wachtwoord.";
                }
            }
        });
    }
    else {
        document.getElementById("alert3").innerHTML = "Vul alle gegevens in.";
    }
    return false;
}
<?php
include('../db.php');
if(isset($_SESSION['tafel_nr'])) {
    $tafel = $_SESSION['tafel_nr'];
}
else if(isset($_POST['tafel'])) {
    $tafel = $_POST['tafel'];
}
$sql = "DELETE FROM tafels WHERE tafel = (:tafel)";
$result = $con->prepare($sql);
$result->bindParam(':tafel', $tafel);
$result->execute();

$sql = "DELETE FROM bestellingen WHERE fk_tafel_id = (:tafel)";
$result = $con->prepare($sql);
$result->bindParam(':tafel', $tafel);
$result->execute();
if(isset($_SESSION['tafel_nr'])) {
    session_unset();     
    session_destroy();   
}
echo "success";


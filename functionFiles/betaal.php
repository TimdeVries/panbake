<?php
include('../db.php');
$betaal = $_POST['betaal'];
$sql = "UPDATE tafels SET betaald = :betaal WHERE tafel = :tafel";
$result = $con->prepare($sql);
$result->bindParam(':tafel', $_SESSION['tafel_nr']);
$result->bindParam(':betaal', $betaal);
$result->execute();
echo "success";
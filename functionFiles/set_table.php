<?php
include('../db.php');
if(isset($_POST['tafel'])) {
    $sql = "SELECT tafel FROM tafels WHERE tafel = :tafel";
    $result = $con->prepare($sql);
    $result->bindParam(':tafel', $_POST['tafel']);
    $result->execute();
    $num_rows = $result->rowCount();
    if($num_rows > 0) {
        echo 'dubbel';
    }

    else if (preg_match('/^\d+$/', $_POST['tafel']) ) {
        $_SESSION['tafel_nr'] = $_POST['tafel'];
        $sql = "INSERT INTO tafels(tafel) VALUES(:tafel)";
        $result = $con->prepare($sql);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->execute();
        echo "success";
    }
    else {
        echo 'nummer';
    }
}




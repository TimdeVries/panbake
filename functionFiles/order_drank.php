<?php
include('../db.php');
if(isset($_POST['drank'])) {
    $drank = $_POST['drank'];
    $sql = "SELECT hoeveel, fk_drank FROM bestellingen WHERE fk_drank = $drank AND bevestigd = 0";
    $result = $con->prepare($sql);
    $result->execute();
    $rows_bestelling = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $fk_drank = $row['fk_drank'];
        $hoeveel = $row['hoeveel'];
    }
    if($fk_drank != NULL) {
        $hoeveel = $hoeveel + 1;
        $sql = "UPDATE bestellingen SET hoeveel = :hoeveel WHERE fk_drank = $drank";
        $result = $con->prepare($sql);
        $result->bindParam(':hoeveel', $hoeveel);
        $result->execute();
    }
    else
    {
        $sql = "INSERT INTO bestellingen(fk_tafel_id, fk_drank, hoeveel) VALUES(:tafel, :drank, 1)";
        $result = $con->prepare($sql);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->bindParam(':drank', $drank);
        $result->execute();
    }
}


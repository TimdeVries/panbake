<?php
include('../db.php');
if(isset($_POST['pass']) && isset($_POST['user'])) {
    $user = trim($_POST['user']);
    $pass = trim($_POST['pass']);
    $sql = "SELECT `wachtwoord` FROM `gebruikers` WHERE `naam` = :user";
    $result = $con->prepare($sql);
    $result->bindParam(':user', $user, PDO::PARAM_STR);
    $result->execute();
    $row = $result->fetch(PDO::FETCH_ASSOC);
    $check = $row['wachtwoord'];
    if($check == $pass)
    {
        $_SESSION['user'] = $user;
        echo "success";
    }
    else {

        echo "fail";
    }
}
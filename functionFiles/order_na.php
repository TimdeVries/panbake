<?php
include('../db.php');
if(isset($_POST['na'])) {
    $na = $_POST['na'];
    $sql = "SELECT hoeveel, fk_na FROM bestellingen WHERE fk_na = $na AND bevestigd = 0";
    $result = $con->prepare($sql);
    $result->execute();
    $rows_bestelling = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $fk_na = $row['fk_na'];
        $hoeveel = $row['hoeveel'];
    }
    if($fk_na != NULL) {
        $hoeveel = $hoeveel + 1;
        $sql = "UPDATE bestellingen SET hoeveel = :hoeveel WHERE fk_na = $na";
        $result = $con->prepare($sql);
        $result->bindParam(':hoeveel', $hoeveel);
        $result->execute();
    }
    else
    {
        $sql = "INSERT INTO bestellingen(fk_tafel_id, fk_na, hoeveel) VALUES(:tafel, :na, 1)";
        $result = $con->prepare($sql);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->bindParam(':na', $na);
        $result->execute();
    }
}
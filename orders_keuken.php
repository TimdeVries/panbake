<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Keuken</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
<?php include('db.php'); ?>
    <div id="body">
    <?php if(!isset($_SESSION['user'])): ?>
    <div id="login">
        <form onsubmit="return do_login();">
            <input type="text" id="user" placeholder="gebruiker">
            <input type="password" id="pass" placeholder="wachtwoord">
            <button type="submit">Log in</button>
        </form>
        <p id="alert3" style="color:red"></p>
    </div>
    <?php endif;
    if(isset($_SESSION['user'])): ?>
    
    <script>
    setTimeout(function() {
        location.reload();
    }, 5000);
    </script>

    <?php
        $sql_bestelling = "SELECT * FROM bestellingen WHERE bereid = 0 AND bevestigd = 1 AND (fk_voor > 0 OR fk_hoofd > 0 OR fk_na > 0)";
        $result = $con->prepare($sql_bestelling);
        $result->execute();
        $rows_bestelling = $result->rowCount();
        while ($row = $result->fetch(PDO::FETCH_BOTH)) {
            $voor[] = $row['fk_voor'];
            $hoofd[] = $row['fk_hoofd'];
            $na[] = $row['fk_na'];
            $hoeveel[] = $row['hoeveel'];
            $date[] = $row['tijd'];
            $fk_tafel_id[] = $row['fk_tafel_id'];
            $bestelling_id[] = $row['id']; 
        }
    ?>
    <button id="bestel" style="float:right;" onclick="return logout();">log uit</button>
    <h1 style="text-align: center;">Bestellingen keuken</h1>
    <?php for ($i = 0; $i < $rows_bestelling; $i++):
    if($hoofd[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM hoofdgerechten WHERE id = $hoofd[$i]";
    }
    if($voor[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM voorgerechten WHERE id = $voor[$i]";
    }
    if($na[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM nagerechten WHERE id = $na[$i]";
    }
    $result = $con->prepare($sql);
    $result->execute();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $bestelling_naam = $row['naam'];
    }
    ?>
    <div id="lijst">
        <button onclick="return bestel_keuken(<?php echo $bestelling_id[$i]; ?>)" class='keuken' id='bestel' >gereed</button>
        <span style="float: right;"><?php echo 'tafel '.$fk_tafel_id[$i].' om '.$date[$i].'&nbsp'; ?></span> 
        <p style="text-align: left;"><?php echo $bestelling_naam.' x '.$hoeveel[$i];?></p>
    </div>
    <?php endfor; endif;?>
    </div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Menu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/menu.js"></script>
</head>
<body>
<?php include('db.php'); ?>
<?php   $sql="SELECT betaald FROM tafels WHERE tafel = :tafel AND betaald = 1 OR betaald = 2";
        $result = $con->prepare($sql);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->execute();
        $row_betaal = $result->rowCount();
        if($row_betaal > 0): ?>
        <script>
        setTimeout(function() {
            location.reload();
        }, 5000);
        </script>
        <p style='text-align:center;'>Er komt zo iemand van de bediening om af te rekenen</p>
    <?php endif;
    if($row_betaal == 0): ?>
    <div id="header" class="container_fluid">
        <div class="row">
            <div class="col-lg-2"><button onclick="betalen();" class="header btn btn-warning betalen">betalen</button></div>
            <?php if(isset($_SESSION['tafel_nr'])): ?>
            <div class="col-lg-8"><div class="header">tafel <?php echo $_SESSION['tafel_nr']; ?></div></div>
            <?php endif;
            if(!isset($_SESSION['tafel_nr'])): ?>
            <div class="col-lg-8">
                <form onsubmit="return tafel();">
                    <label>voer tafelnummer in</label>
                    <input id="tafel_nummer" class='tafel_form' type="text">
                    <button class='tafel_form' type="submit">ok</button>
                </form>
                <p style="color:red; text-align:center;" id='alert_tafel'></p>
            </div>
            <?php endif; ?>
            <div class="col-lg-2"><button onclick="bestellen()" class="header btn btn-warning bestelling">bestelling</button></div>
        </div>
        <div class="row">
            <div class="col-lg-3"><button onclick="voor()" id="voor" class="btn btn-warning">voorgerechten</button></div>
            <div class="col-lg-3"><button onclick="hoofd()" id="hoofd" class="btn btn-warning">pannenkoeken</button></div>
            <div class="col-lg-3"><button onclick="na()" id="na" class="btn btn-warning">nagerechten</button></div>
            <div class="col-lg-3"><button onclick="drank()" id="drank" class="btn btn-warning">dranken</button></div>
        </div>
    </div>
    <?php

    $sql_drank = "SELECT * FROM dranken";
    $result = $con->prepare($sql_drank);
    $result->execute();
    $rows_drank = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $drank_naam[] = $row['naam'];
        $drank_prijs[] = $row['prijs'];
        $drank_id[] = $row['id'];
    }
    $sql_voor = "SELECT * FROM voorgerechten";
    $result = $con->prepare($sql_voor);
    $result->execute();
    $rows_voor = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $voor_naam[] = $row['naam'];
        $voor_prijs[] = $row['prijs'];
        $voor_id[] = $row['id'];
    }
    $sql_hoofd = "SELECT * FROM hoofdgerechten";
    $result = $con->prepare($sql_hoofd);
    $result->execute();
    $rows_hoofd = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $hoofd_naam[] = $row['naam'];
        $hoofd_prijs[] = $row['prijs'];
        $hoofd_id[] = $row['id'];
    }
    $sql_na = "SELECT * FROM nagerechten";
    $result = $con->prepare($sql_na);
    $result->execute();
    $rows_na = $result->rowCount();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $na_naam[] = $row['naam'];
        $na_prijs[] = $row['prijs'];
        $na_id[] = $row['id'];
    }

    ?>
    <div id="menu">
    <div id="div_drank">
        <table id="table_drank">
            <tr>
                <th>Dranken</th>
                <th>Prijs</th>
                <th>Kies</th>
            </tr>
            <?php for ($i = 0; $i < $rows_drank; $i++): ?>
            <tr>
                <td><?php echo $drank_naam[$i]; ?>
                <td><?php echo $drank_prijs[$i]; ?>
                <td>
                    <button id="bestel" onclick="return bestel_drank('<?php echo $drank_id[$i]; ?>')">Bestel</button>
                </td>
            </tr>
            <?php endfor; ?>
        </table>
    </div>
    <div id="div_voor">
        <table id="table_voor">
            <tr>
                <th>Voorgerechten</th>
                <th>Prijs</th>
                <th>Kies</th>
            </tr>
            <?php for ($i = 0; $i < $rows_voor; $i++): ?>
            <tr>
                <td><?php echo $voor_naam[$i]; ?>
                <td><?php echo $voor_prijs[$i]; ?>
                <td>
                    <button id="bestel" onclick="return bestel_voor('<?php echo $voor_id[$i]; ?>')">Bestel</button>
                </td>
            </tr>
            <?php endfor; ?>
        </table>
    </div>
    <div id="div_hoofd">
        <table id="table_hoofd">
            <tr>
                <th>Hoofdgerechten</th>
                <th>Prijs</th>
                <th>Kies</th>
            </tr>
            <?php for ($i = 0; $i < $rows_hoofd; $i++): ?>
            <tr>
                <td><?php echo $hoofd_naam[$i]; ?>
                <td><?php echo $hoofd_prijs[$i]; ?>
                <td>
                    <button id="bestel" onclick="return bestel_hoofd('<?php echo $hoofd_id[$i]; ?>')">Bestel</button>
                </td>
            </tr>
            <?php endfor; ?>
        </table>
    </div>
    <div id="div_na">
        <table id="table_na">
            <tr>
                <th>Nagerechten</th>
                <th>Prijs</th>
                <th>Kies</th>
            </tr>
            <?php for ($i = 0; $i < $rows_na; $i++): ?>
            <tr>
                <td><?php echo $na_naam[$i]; ?>
                <td><?php echo $na_prijs[$i]; ?>
                <td>
                    <button id="bestel" onclick="return bestel_na('<?php echo $na_id[$i]; ?>')">Bestel</button>
                </td>
            </tr>
            <?php endfor; ?>
        </table>
    </div>
    </div>
    <!-- hieronder word de pop-up voor bestellingen bevestigen gemaakt. -->
    <?php
        $sql_bestelling = "SELECT * FROM bestellingen WHERE fk_tafel_id = :tafel AND bevestigd = 0";
        $result = $con->prepare($sql_bestelling);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->execute();
        $rows_bestelling = $result->rowCount();
        while ($row = $result->fetch(PDO::FETCH_BOTH)) {
            $drank[] = $row['fk_drank'];
            $voor[] = $row['fk_voor'];
            $hoofd[] = $row['fk_hoofd'];
            $na[] = $row['fk_na'];
            $hoeveel[] = $row['hoeveel'];
            $id[] = $row['id'];
        }
    ?>
    <div id="bestelling">
        <h1 style="text-align: center;">Uw bestelling</h1>
        <p id="close" onclick ="div_hide()">X</p>
        <div id="lijst">
            <?php for ($i = 0; $i < $rows_bestelling; $i++):
            if($drank[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM dranken WHERE id = $drank[$i]";
            }
            if($hoofd[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM hoofdgerechten WHERE id = $hoofd[$i]";
            }
            if($voor[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM voorgerechten WHERE id = $voor[$i]";
            }
            if($na[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM nagerechten WHERE id = $na[$i]";
            }
            $result = $con->prepare($sql);
            $result->execute();
            while ($row = $result->fetch(PDO::FETCH_BOTH)) {
                $bestelling_naam = $row['naam'];
                $bestelling_prijs = $row['prijs'];
            }
            $bestelling_prijs = $bestelling_prijs * $hoeveel[$i];
            $bestelling_prijs = number_format($bestelling_prijs, 2, '.', '');
            ?>
            <span style="float: right;"><?php echo $bestelling_prijs; ?></span>
            <p id="change" onclick="return change(<?php echo $id[$i]; ?>, -1, <?php echo $hoeveel[$i]; ?>)" class="change">-</p> 
            <p class="change"><?php echo $bestelling_naam.' x '.$hoeveel[$i];?></p>
            <p id="change" onclick="return change(<?php echo $id[$i]; ?>, 1, <?php echo $hoeveel[$i]; ?>)" class="change">+</p>
            <br>
            <?php endfor;?>
            <button onclick="return bevestigen();" class="header btn btn-warning bevestigen">bevestig bestelling</button>
        </div>
    </div>
    <!-- hieronder word de pop-up voor afrekenen gemaakt. -->
    <?php
        $sql_bestelling2 = "SELECT * FROM bestellingen WHERE fk_tafel_id = :tafel";
        $result = $con->prepare($sql_bestelling2);
        $result->bindParam(':tafel', $_SESSION['tafel_nr']);
        $result->execute();
        $rows_bestelling2 = $result->rowCount();
        while ($row = $result->fetch(PDO::FETCH_BOTH)) {
            $drank2[] = $row['fk_drank'];
            $voor2[] = $row['fk_voor'];
            $hoofd2[] = $row['fk_hoofd'];
            $na2[] = $row['fk_na'];
            $hoeveel2[] = $row['hoeveel'];
        }
    ?>
    <div id="betalen">
        <h1 style="text-align: center;">Uw bestelling</h1>
        <p id="close" onclick ="div_hide2()">X</p>
        <div id="lijst2">
            <?php if ($rows_bestelling2 > 0): for ($i = 0; $i < $rows_bestelling2; $i++): 
            if($drank2[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM dranken WHERE id = $drank2[$i]";
            }
            if($hoofd2[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM hoofdgerechten WHERE id = $hoofd2[$i]";
            }
            if($voor2[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM voorgerechten WHERE id = $voor2[$i]";
            }
            if($na2[$i] != NULL) {
                $sql = "SELECT naam, prijs FROM nagerechten WHERE id = $na2[$i]";
            }
            $result = $con->prepare($sql);
            $result->execute();
            while ($row = $result->fetch(PDO::FETCH_BOTH)) {
                $bestelling_naam2[] = $row['naam'];
                $bestelling_prijs2[] = $row['prijs'];
            }
            $bestelling_prijs2[$i] = $bestelling_prijs2[$i] * $hoeveel2[$i];
            $bestelling_prijs2[$i] = number_format($bestelling_prijs2[$i], 2, '.', '');
            ?>
            <span style="float: right;"><?php echo $bestelling_prijs2[$i]; ?></span> 
            <p style="text-align: left;"><?php echo $bestelling_naam2[$i].' x '.$hoeveel2[$i];?></p>
            <?php endfor;
            $totale_prijs = array_sum($bestelling_prijs2);
            $totale_prijs = number_format($totale_prijs, 2, '.', '');
            ?>
            <div style="border-top: 1px solid black;">
                <span style="float: right;"><?php echo $totale_prijs; ?></span> 
                <p style="text-align: left;">totaal</p>
            </div>
            <?php endif; 
            $sql = "SELECT gebracht FROM bestellingen WHERE gebracht = 0";
            $result = $con->prepare($sql);
            $result->execute();
            $rows_gebracht = $result->rowCount();
            if($rows_gebracht == 0 && $rows_bestelling2 == 0):
            ?>
            <p style="text-align: center;">U heeft nog niks besteld, als u straks wilt afrekenen kan dat contant, met de pin of met ideal.</p>
            <?php endif;
            if($rows_gebracht == 0 && $rows_bestelling2 > 0):
            ?>
            <div class="row">
                <div class="col-lg-4"><button onclick="betaal_anders(1)" id="voor" class="btn btn-warning">contant</button></div>
                <div class="col-lg-4"><button onclick="betaal_anders(2)" id="hoofd" class="btn btn-warning">pinnen</button></div>
                <div class="col-lg-4"><button onclick="betaal()" id="na" class="btn btn-warning">ideal</button></div>
            </div>
            <?php endif;
            if($rows_gebracht > 0): ?>
            <p style="text-align: center;">U heeft nog niet al uw bestellingen ontvangen.</p>
            <?php endif; endif; ?>
        </div>
    </div>
</body>
</html>
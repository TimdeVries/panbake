<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bediening</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
<?php include('db.php'); ?>
<script>
    function bestel_keuken(keuken) {
        $.ajax
        ({
            type:'post',
            url:'functionFiles/order_bediening.php',
            data: {
                    keuken:keuken
                },
                success:function(response) {
                if(response=="success")
                {
                    refresh();
                }
            }
        });
    }

    function betaal(tafel) {
        $.ajax
        ({
            type:'post',
            url:'functionFiles/unset_table.php',
            data: {
                        tafel:tafel
                    },
            success:function(response) {
                if(response=="success")
                {
                    refresh();
                }
            }
        });
    }
    </script>
    <div id="body">
    <?php if(!isset($_SESSION['user'])): ?>
    <div id="login">
        <form onsubmit="return do_login();">
            <input type="text" id="user" placeholder="gebruiker">
            <input type="password" id="pass" placeholder="wachtwoord">
            <button type="submit">Log in</button>
        </form>
        <p id="alert3" style="color:red"></p>
    </div>
    <?php endif;
    if(isset($_SESSION['user'])): ?>
        
    <script>
    setTimeout(function() {
        location.reload();
    }, 5000);
</script>
    
    <?php
        $sql_bestelling = "SELECT * FROM bestellingen WHERE bereid = 1 AND gebracht = 0";
        $result = $con->prepare($sql_bestelling);
        $result->execute();
        $rows_bestelling = $result->rowCount();
        while ($row = $result->fetch(PDO::FETCH_BOTH)) {
            $drank[] = $row['fk_drank'];
            $voor[] = $row['fk_voor'];
            $hoofd[] = $row['fk_hoofd'];
            $na[] = $row['fk_na'];
            $hoeveel[] = $row['hoeveel'];
            $date[] = $row['tijd'];
            $fk_tafel_id[] = $row['fk_tafel_id'];
            $bestelling_id[] = $row['id']; 
        }

        $sql = "SELECT tafel, betaald FROM tafels WHERE betaald = 1 OR betaald = 2";
        $result = $con->prepare($sql);
        $result->execute();
        $rows_betalen = $result->rowCount();
        while ($row = $result->fetch(PDO::FETCH_BOTH)) {
            $tafel[] = $row['tafel'];
            $betaald[] = $row['betaald'];
        }
    ?>
    <button id="bestel" style="float:right;" onclick="return logout();">log uit</button>
    <h1 style="text-align: center;">Bestellingen bediening</h1>
    <?php 
    for($i = 0; $i < $rows_betalen; $i++): 
    if ($betaald[$i] == 1) {
        $betaald[$i] = "contant";
    }
    else if($betaald[$i == 2]) {
        $betaald[$i] = "pinnen";
    }
    ?>
    <div id="lijst">
        <button onclick="return betaal(<?php echo $tafel[$i]; ?>)" class='keuken' id='bestel' >betaald</button>
        <span style="float: right;"><?php echo 'tafel '.$tafel[$i].'&nbsp'; ?></span> 
        <p style="text-align: left;"><?php echo $betaald[$i];?></p>
    </div>
    <?php
    endfor;
    for ($i = 0; $i < $rows_bestelling; $i++):
    if($drank[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM dranken WHERE id = $drank[$i]";
    }
    if($hoofd[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM hoofdgerechten WHERE id = $hoofd[$i]";
    }
    if($voor[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM voorgerechten WHERE id = $voor[$i]";
    }
    if($na[$i] != NULL) {
        $sql = "SELECT naam, prijs FROM nagerechten WHERE id = $na[$i]";
    }
    $result = $con->prepare($sql);
    $result->execute();
    while ($row = $result->fetch(PDO::FETCH_BOTH)) {
        $bestelling_naam = $row['naam'];
    }
    ?>
    <div id="lijst">
        <button onclick="return bestel_keuken(<?php echo $bestelling_id[$i]; ?>)" class='keuken' id='bestel' >gereed</button>
        <span style="float: right;"><?php echo 'tafel '.$fk_tafel_id[$i].' om '.$date[$i].'&nbsp'; ?></span> 
        <p style="text-align: left;"><?php echo $bestelling_naam.' x '.$hoeveel[$i];?></p>
    </div>
    <?php endfor; endif;?>
    </div>
</body>
</html>
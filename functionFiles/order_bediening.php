<?php
include('../db.php');
$date = date('Y-m-d H:i:s');
if(isset($_POST['keuken'])) {
    $keuken = $_POST['keuken'];
    $sql = "UPDATE bestellingen SET gebracht = 1, tijd = :tijd WHERE id = :keuken";
    $result = $con->prepare($sql);
    $result->bindParam(':keuken', $keuken);
    $result->bindParam(':tijd', $date);
    $result->execute();
    echo "success";
}